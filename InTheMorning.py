#!/usr/bin/env python

# copyright 2019 Jezra
# Station
# Licensed GPL v3

''' import '''
import sys, os, time
import subprocess
from shlex import quote

#use gevent, and monkey patch!
from gevent import monkey
monkey.patch_all() #patch early

# import bottle stuff https://bottlepy.org/
from bottle import Bottle, run, static_file, request, abort

#where is this script?
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
#change to the Script directory
os.chdir(SCRIPT_DIR)

#get the config
import Config

#get the item list utilities
import Items

# create a bottle app
app = Bottle()

# track the state of processing
PROCESSING = {}

#load the items from disk
for time in Config.TIMES:
  Items.load(time)
  #set processing to false
  PROCESSING[time] = False

@app.route('/')
def root():
  return serve_static("index.html")

@app.route('/<filepath:path>')
def serve_static(filepath):
  response = static_file(filepath, root='public')
  response.set_header("Cache-Control", "no-store")
  return response

@app.route('/times', method='GET')
def get_times():
  return {"times": Config.TIMES}

@app.route('/add', method='PUT')
def put_tell_me():
  item = request.json.get('item')
  time = request.json.get('time')

  #is the time valid?
  if time not in Config.TIMES:
    abort(400, 'bad request')

  Items.add(item,time)
  ret = Items.items
  return {"items":ret}

@app.route('/remove', method='PUT')
def put_tell_me():
  item = request.json.get('item')
  time = request.json.get('time')
  #is the time valid?
  if time not in Config.TIMES:
    abort(400, 'bad request')

  Items.remove(item,time)
  ret = Items.items
  return {"items":ret}

@app.route('/process', method='PUT')
def put_process():
  global PROCESSING
  time = request.json.get('time')

  #if processing is not happening
  if not PROCESSING[time]:
    PROCESSING[time]=True
    items = Items.items[time]
    for item in items:
      if item != None and len(item)>0:
        print("found: {}".format(item))
        #sanitize the item string
        sanitized_item = quote(item)
        # create the processing command line
        cmd_line = "{} {}".format(Config.COMMAND, sanitized_item)
        print(cmd_line)
        # run the command
        subprocess.run([cmd_line],shell=True)
    #clear the items
    Items.clear(time)
    PROCESSING[time]=False

  ret = Items.items
  return {"items":ret}

@app.route('/items', method='GET')
def get_items():
  ret = Items.items
  return {"items":ret}

#run the server app
app.run( host='0.0.0.0', port=Config.PORT, server='gevent')

