"use strict";
let items = [];
//make it less verbose to find by id
function $(id){ return document.getElementById(id); }

//make it less verbose to create an element
function $c(tag){ return document.createElement(tag); }

//on page load, start checking the status
function init(){
  updateItemList();
  updateTimes();
}

function clickedSubmit(){
  
  var item = $('new-item').value;
  var time = $('time-select').value;
  //don't submit if there is no item
  if (item.length>0) {
    fetch("/add", {
      method: "PUT", 
      body: JSON.stringify({"item":item,"time":time}),
      headers: {
        "Content-Type": "application/json",
      },
    }).then( (response)=>{
      // get the json from the response 
      response.json().then(function(json) {
        // update 
        updateUIItemList(json.items);
      });
      $('new-item').value='';
    });
  }
  return false;
}

// get the times from the API
function updateTimes () {
  fetch("/times",{
    method:"GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then( (response)=>{
    // get the json from the response 
    response.json().then(function(json) {
      // update 
      updateUITimesOptions(json.times);
    });   
  });
}

function updateItemList(){
  fetch("/items",{
    method:"GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then( (response)=>{
    // get the json from the response 
    response.json().then(function(json) {
      // update 
      updateUIItemList(json.items);
    });  
    
  });
}

function updateUITimesOptions(timesList){
  //clear the list
  let timesUI = $("time-select");
 timesUI.innerHTML = '';
  timesList.forEach((time)=>{
    let timeOption = $c("option");
    timeOption.setAttribute('value',time);
    timeOption.innerHTML = time;
    timesUI.appendChild(timeOption);
  });
}

function updateUIItemList(itemTimes){
  //clear the list
  let itemsUI = $("items");
  itemsUI.innerHTML = '';

  //get the keys from itemTimes object
  let keys = Object.keys(itemTimes);
  // loop through the keys
  keys.forEach((key)=>{
    //create the time header
    let timeDisplay = $c('h3');
    timeDisplay.innerHTML=key;
    //append to the dom
    itemsUI.appendChild(timeDisplay);
    
    //get and loop through the key's items array
    itemTimes[key].forEach((item)=>{
      let itemUI = $c("div");
      itemUI.classList.add('item');
      let text = document.createTextNode(item);
      let delButton = $c("button");
      delButton.setAttribute('data-item',item);
      delButton.setAttribute('data-time',key);
      delButton.setAttribute('title',"delete "+item);
      delButton.classList.add('delete-button');
      delButton.addEventListener('click', clickedDeleteButton);
      delButton.innerHTML = "&#128465";
      itemUI.appendChild(text);
      itemUI.appendChild(delButton);
      itemsUI.appendChild(itemUI);
    });
  });
}

function clickedDeleteButton(event){
  var item = event.target.dataset.item;
  var time = event.target.dataset.time;
  //verify
  if(!confirm(`Remove '${item}' from '${time}'?`)) {
    return;
  }
  fetch("/remove", {
    method: "PUT", // *GET, POST, PUT, DELETE, etc.
    body: JSON.stringify({"item":item,"time":time}),
    headers: {
      "Content-Type": "application/json",
    },
  }).then( (response)=>{
    //clear the input form
    $('new-item').value = '';
    // get the json from the response 
    response.json().then(function(json) {
      // update 
      updateUIItemList(json.items);
    });
  });
}
