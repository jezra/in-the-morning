# Copright 2019 Jezra
# Released under the GPLv3
# see: http://www.gnu.org/licenses/gpl-3.0.html
from os import path

key = 'items.txt'
items = {}

def load(key):
  global items
  #truncate the item list
  items[key] = []
  for item in file_read(key):
    if len(item) > 0:
      items[key].append(item)

def file_write(key):
  handler = open(key, 'w')
  handler.write("\n".join(items[key]))
  handler.close()

def file_read(key):
  #does the file exist?
  if path.isfile(key):
    handler = open(key, 'r')
    text = handler.read()
    handler.close()
    return text.split("\n")
  else:
    return [] #return an empty array

def add(text,key):
  global items
  if text!=None and len(text)>0:
    #load the existing items
    load(key)
    #add the new item
    items[key].append(text)
    file_write(key)

def remove(text,key):
  global items
  if text != None:
    new_list = []
    for item in items[key]:
      if item != text:
        new_list.append(item)
    items[key] = new_list
    file_write(key)

def clear(key):
  global items
  items[key] = []
  file_write(key)
