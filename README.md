# In The Morning

ITM is a basic web server that provides a network accessible interface to a list of text strings. The interface is accessed via a web browser and allows the user to add and delete strings. The interface also allows the user to process the text strings using a configurable shell script.

## Configuration
Configuration is handled by the Config.py file.

copy Config.py.example to Config.py and edit accordingly

### Options
 * PORT: the port number for the server
* COMMAND: the command to use when processing the text strings
* TIMES: an array of strings that represent a time that processing of some items should happen


## Usage

to Run 'in-the-morning':

* open the terminal
* `cd` to the directory with the source code
* run `./InTheMorning.py` 

 

### Triggering 'time' processing

to trigger the processing of the items set for a given time, it is necessary to sent a PUT request to `http://HOSTNAME:CONFIG_PORT/process` and include the string name of the time to process as a json parameter named "time".

For example, using the default configuration on localhost, processing the "in the evening" time items would require a PUT request to `http://localhost:8081/process` with a json payload of `{"time":"in the evening"}`

**curl example**

    curl -H 'Content-Type: application/json' \
    -d '{"time":"in the evening"}' \
    -X PUT http://localhost:8081/process
 
In my setup, this curl script is run as a cron job every day at 5:45PM
